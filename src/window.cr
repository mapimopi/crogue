module Window
  def self.create
    LibRay.set_config_flags LibRay::FLAG_WINDOW_RESIZABLE
    LibRay.init_window 800, 600, "Testing"
    LibRay.set_target_fps 60
  end

  def self.destroy
    LibRay.close_window
  end

  def self.width
    LibRay.get_screen_width
  end

  def self.height
    LibRay.get_screen_height
  end
end
