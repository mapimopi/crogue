class Gameplay::Presentation
  private def initialize_tiles
    LibRay.set_texture_filter @tiles_texture,   LibRay::TextureFilterMode::FILTER_POINT
    LibRay.set_texture_filter @sprites_texture, LibRay::TextureFilterMode::FILTER_POINT
  end

  private def image_path(filename)
    File.join ASSETS_PATH, "images", filename
  end
end
