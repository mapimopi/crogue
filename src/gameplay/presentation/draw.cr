class Gameplay::Presentation
  private def draw_tile(tile : Int32, x, y)
    LibRay.draw_texture_rec \
      @tiles_texture,
      source_rec(tile, TILE_SIZE),
      dest_vec(x * TILE_SIZE, y * TILE_SIZE),
      LibRay::WHITE
  end

  private def draw_sprite(tile : Int32, x, y, offset_x = 0.0, offset_y = 0.0, flip = false)
    LibRay.draw_texture_pro \
      @sprites_texture,
      source_rec(tile, SPRITE_SIZE, flip),
      dest_rec(x * TILE_SIZE + offset_x - 2, y * TILE_SIZE + offset_y - 4),
      dest_vec(0, 0),
      0.0,
      LibRay::WHITE
  end

  private def source_rec(tile : Int32, size : Int32, flip : Bool = false)
    @source_rec.x = (tile % TEXTURE_ROW) * size
    @source_rec.y = (tile / TEXTURE_ROW) * size
    @source_rec.width  = size
    @source_rec.height = size

    if flip
      @source_rec.width = -size
    end

    return @source_rec
  end

  private def dest_vec(x, y)
    @dest_vec.x = x
    @dest_vec.y = y

    return @dest_vec
  end

  private def dest_rec(x, y)
    @dest_rec.x = x
    @dest_rec.y = y
    @dest_rec.width  = SPRITE_SIZE
    @dest_rec.height = SPRITE_SIZE

    return @dest_rec
  end
end
