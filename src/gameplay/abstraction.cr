class Gameplay::Abstraction < MAPI::Abstraction
  getter \
    map = Map.new,
    world = World.new,
    camera = Camera.new

  def initialize
    @world.spawn_player

    999.times do
      @world.spawn_fake_player
    end
  end

  def update
    @world.update
    @camera.update
  end
end
