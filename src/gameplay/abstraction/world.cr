class Gameplay::Abstraction::World < ECS::World
  ecs \
    components: [
      Component::Position,
      Component::Sprite
    ],
    systems: [
      System::CameraFollow,
      System::PlayerMovement
    ]
end
