class System::PlayerMovement < ECS::System
  filter :player, :position, :sprite

  def update(id)
    position = @world.component_position id
    sprite = @world.component_sprite id

    if Game.input.left
      position.x -= 1 if sprite.flip == false
      sprite.flip = false
    elsif Game.input.right
      position.x += 1 if sprite.flip == true
      sprite.flip = true
    end

    if Game.input.down
      position.y += 1
    elsif Game.input.jump_up
      position.y -= 1
    elsif Game.input.jump_left
      if sprite.flip == false
        position.x -= 1
        position.y -= 1
      end
      sprite.flip = false
    elsif Game.input.jump_right
      if sprite.flip == true
        position.x += 1
        position.y -= 1
      end
      sprite.flip = true
    end
  end
end
