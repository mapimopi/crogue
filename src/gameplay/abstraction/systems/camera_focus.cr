class System::CameraFollow < ECS::System
  filter :camera_focus, :position

  def update(id)
    position = @world.component_position id

    Game.abstraction.camera.move_to \
      position.x * TILE_SIZE,
      position.y * TILE_SIZE
  end
end
