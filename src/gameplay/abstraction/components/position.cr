class Component::Position < ECS::Component
  property \
    x : Int32 = 0,
    y : Int32 = 0
end
