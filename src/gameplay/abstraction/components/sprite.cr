class Component::Sprite < ECS::Component
  property \
    tile : Int32 = 0,
    offset_x : Float32 = 0.0,
    offset_y : Float32 = 0.0,
    flip : Bool = false
end
