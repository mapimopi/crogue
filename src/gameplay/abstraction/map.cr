class Gameplay::Abstraction::Map
  def initialize
    @cells = Pointer(Cell).malloc(MAP_SIZE * MAP_SIZE)
    @empty = Cell.new(0)

    MAP_SIZE.times do |x|
      MAP_SIZE.times do |y|
        @cells[x * MAP_SIZE + y] = Cell.new(LibRay.get_random_value(0, 15))
      end
    end
  end

  def [](x, y)
    return @empty if out_of_bounds? x, y
    @cells[x * MAP_SIZE + y]
  end

  private def out_of_bounds?(x, y)
    x < 0 || x >= MAP_SIZE || y < 0 || y >= MAP_SIZE
  end
end
