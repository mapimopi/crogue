class Gameplay::Abstraction::Camera
  POSITION_SMOOTHING = 0.1
  ZOOM_MIN = 3.0
  ZOOM_MAX = 10.0

  getter \
    target = Vector2.new,
    position = Vector2.new,
    zoom = 4.0

  def update
    move_by Game.input.camera_x, Game.input.camera_y
    zoom_by Game.input.camera_zoom
    update_position
  end

  def move_to(x, y, instant = false)
    @target.x = x
    @target.y = y

    if instant
      @position.x = x
      @position.y = y
    end
  end

  def move_by(x, y)
    @target.x = @target.x + x
    @target.y = @target.y + y
  end

  def set_zoom(value)
    @zoom = value.clamp(ZOOM_MIN, ZOOM_MAX)
  end

  def zoom_by(value)
    @zoom = (@zoom + value).clamp(ZOOM_MIN, ZOOM_MAX)
  end

  private def update_position
    @position.x = Tween.step @position.x, @target.x, POSITION_SMOOTHING
    @position.y = Tween.step @position.y, @target.y, POSITION_SMOOTHING
  end

  def cell_bounds
    center      = center_cell
    half_width  = width_in_cells  / 2
    half_height = height_in_cells / 2
    tolerance   = 2

    CameraBounds.new \
      left:   center.x.to_i - half_width.to_i  - tolerance,
      right:  center.x.to_i + half_width.to_i  + tolerance,
      top:    center.y.to_i - half_height.to_i - tolerance,
      bottom: center.y.to_i + half_height.to_i + tolerance
  end

  private def center_cell
    Vector2.new \
      x: ( @position.x + TILE_SIZE / 2 ) / TILE_SIZE,
      y: ( @position.y + TILE_SIZE / 2 ) / TILE_SIZE
  end

  private def width_in_cells
    Window.width / TILE_SIZE / @zoom
  end

  private def height_in_cells
    Window.height / TILE_SIZE / @zoom
  end
end
