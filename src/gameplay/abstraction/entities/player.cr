class Gameplay::Abstraction::World
  def spawn_player
    id = spawn \
      :player,
      :position,
      :sprite,
      :camera_focus

    component_position(id).x = 100
    component_position(id).y = 100
    component_sprite(id).tile = 2
    component_sprite(id).flip = true
  end

  def spawn_fake_player
    id = spawn \
      :position,
      :sprite

    component_position(id).x = 100 + (( rand + 1 ) * 100 - 50).ceil.to_i
    component_position(id).y = 100 + (( rand + 1 ) * 100 - 50).ceil.to_i
    component_sprite(id).tile = 0
  end
end
