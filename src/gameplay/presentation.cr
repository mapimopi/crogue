class Gameplay::Presentation < MAPI::Presentation
  def initialize
    @tiles_texture   = LibRay.load_texture image_path "tiles.png"
    @sprites_texture = LibRay.load_texture image_path "sprites.png"
    @camera2d        = LibRay::Camera2D.new
    @camera_bounds   = CameraBounds.new 0, 0, 0, 0
    @source_rec      = LibRay::Rectangle.new
    @dest_rec        = LibRay::Rectangle.new
    @dest_vec        = LibRay::Vector2.new

    initialize_tiles
  end

  def update
    update_camera
    begin_drawing
    render_map
    render_entities
    end_drawing
  end

  private def update_camera
    camera             = Game.abstraction.camera
    target_x           = camera.position.x.round 1
    target_y           = camera.position.y.round 1
    half_screen_width  = Window.width  / 2
    half_screen_height = Window.height / 2
    half_cell_width    = TILE_SIZE / 2 * camera.zoom
    half_cell_height   = TILE_SIZE / 2 * camera.zoom

    @camera2d.target.x = target_x
    @camera2d.target.y = target_y
    @camera2d.offset.x = half_screen_width  - half_cell_width  - target_x
    @camera2d.offset.y = half_screen_height - half_cell_height - target_y
    @camera2d.zoom = camera.zoom

    @camera_bounds = camera.cell_bounds
  end

  private def begin_drawing
    LibRay.begin_drawing
    LibRay.clear_background LibRay::BLACK
    LibRay.begin_mode_2d @camera2d
  end

  private def render_map
    @camera_bounds.horizontal.each do |cell_x|
      @camera_bounds.vertical.each do |cell_y|
        draw_tile \
          Game.abstraction.map[cell_x, cell_y].type,
          cell_x,
          cell_y
      end
    end
  end

  private def render_entities
    world = Game.abstraction.world

    world.entities.each_with_index do |entity, id|
      if world.has_components? id, :sprite, :position
        sprite   = world.component_sprite id
        position = world.component_position id

        draw_sprite \
          sprite.tile,
          position.x,
          position.y,
          sprite.offset_x,
          sprite.offset_y,
          sprite.flip
      end
    end
  end

  private def end_drawing
    LibRay.end_mode_2d
    # LibRay.draw_circle_lines Window.width / 2, Window.height / 2, 2, LibRay::RED
    LibRay.draw_text LibRay.get_fps.to_s, Window.width - 12, 0, 10, LibRay::GRAY
    LibRay.end_drawing
  end

  def destroy
    LibRay.unload_texture @tiles_texture
    LibRay.unload_texture @sprites_texture
  end
end
