class Gameplay::Input < MAPI::Input
  getter \
    left  = false,
    right = false,
    down  = false,
    jump_left  = false,
    jump_up    = false,
    jump_right = false,

    camera_x = 0.0,
    camera_y = 0.0,
    camera_zoom = 0.0

  def update
    player_movement
    camera_debug
  end

  private def player_movement
    @left  = LibRay.key_pressed? LibRay::KEY_A
    @right = LibRay.key_pressed? LibRay::KEY_D
    @down  = LibRay.key_pressed? LibRay::KEY_S
    @jump_left  = LibRay.key_pressed? LibRay::KEY_Q
    @jump_up    = LibRay.key_pressed? LibRay::KEY_W
    @jump_right = LibRay.key_pressed? LibRay::KEY_E
  end

  private def camera_debug
    move_speed = 8
    zoom_speed = 0.1

    @camera_x = 0.0
    @camera_y = 0.0
    @camera_zoom = 0.0

    @camera_x -= move_speed if LibRay.key_down? LibRay::KEY_H
    @camera_x += move_speed if LibRay.key_down? LibRay::KEY_L
    @camera_y -= move_speed if LibRay.key_down? LibRay::KEY_K
    @camera_y += move_speed if LibRay.key_down? LibRay::KEY_J

    @camera_zoom += zoom_speed if LibRay.key_down? LibRay::KEY_U
    @camera_zoom -= zoom_speed if LibRay.key_down? LibRay::KEY_I
  end
end
