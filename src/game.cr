module Game
  @@manager : MAPI::Manager?

  def self.start
    @@manager = Gameplay::Manager.new
  end

  def self.loop
    while running?
      @@manager.try &.update
    end
  end

  def self.finish
    @@manager.try &.destroy
  end

  private def self.running?
    !LibRay.window_should_close?
  end

  def self.input : MAPI::Input
    MAPI::Input.cast @@manager.try &.input
  end

  def self.abstraction : MAPI::Abstraction
    MAPI::Abstraction.cast @@manager.try &.abstraction
  end

  def self.presentation : MAPI::Presentation
    MAPI::Presentation.cast @@manager.try &.presentation
  end
end
