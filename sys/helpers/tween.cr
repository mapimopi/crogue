module Tween
  def self.step(from, to, smoothing)
    from + (to - from) * smoothing
  end
end
