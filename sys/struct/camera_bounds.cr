struct CameraBounds
  property! \
    left,
    right,
    top,
    bottom,
    vertical,
    horizontal

  def initialize(
      @left : Int32,
      @right : Int32,
      @top : Int32,
      @bottom : Int32)
    @horizontal = @left..@right
    @vertical   = @top..@bottom
  end
end
