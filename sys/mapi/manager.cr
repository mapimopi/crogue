abstract class MAPI::Manager
  abstract def initialize
  abstract def update
  abstract def destroy
end

macro mapi_manager(class_name)
  class {{ class_name }}::Manager < MAPI::Manager
    property \
      input,
      abstraction,
      presentation

    def initialize
      @input        = Input.new
      @abstraction  = Abstraction.new
      @presentation = Presentation.new
    end

    def update
      @input.update
      @abstraction.update
      @presentation.update
    end

    def destroy
      @input.destroy
      @abstraction.destroy
      @presentation.destroy
    end
  end
end
