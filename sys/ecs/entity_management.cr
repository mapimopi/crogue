module ECS::EntityManagement
  # Create a new entity with a set of components
  def spawn(components : Array(Symbol))
    id = next_entity_index
    @entities[id] = components.to_set
    return id
  end

  # ditto
  def spawn(*components : Symbol)
    spawn(components.to_a)
  end

  # Strip an entity under a given id of all components, rendering it inactive
  def kill(entity_id : Int32)
    @entities[entity_id].clear
  end

  # Check if entity has any components
  def is_alive?(entity_id : Int32)
    @entities[entity_id].size > 0
  end

  # Check if entity has a component
  def has_component?(entity_id : Int32, component_name : Symbol)
    @entities[entity_id].includes? component_name
  end

  def has_components?(entity_id : Int32, components : Set(Symbol))
    components.subset? @entities[entity_id]
  end

  def has_components?(entity_id : Int32, *components : Symbol)
    has_components?(entity_id, components.to_set)
  end
end
