abstract class ECS::System
  getter required_components : Set(Symbol) = Set(Symbol).new

  def initialize(@world : ECS::World); end

  abstract def update(id : Int32)

  # TODO: A way for a system to emit events and subscribe to events.
  # One central event system should be stored inside World, which
  # should call all subscribed event observers callbacks.
end

macro filter(*component_names)
  getter required_components = Set{{ component_names }}
end
