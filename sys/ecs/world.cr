abstract class ECS::World
  include EntityManagement

  getter \
    entities = StaticArray(Set(Symbol), ENTITY_COUNT).new { Set(Symbol).new },
    components = Hash(Symbol, StaticArray(Component, ENTITY_COUNT)).new,
    systems = [] of System

  def update
    @systems.each do |system|
      @entities.each_with_index do |entity, id|
        if has_components? id, system.required_components
          system.update id
        end
      end
    end
  end

  private def next_entity_index
    @entities
      .index { |e| e.size.zero? }
      .tap { |id|
        if id.nil?
          raise "Tried to spawn a new entity when max entity count is reached"
        end
      }
      .not_nil!
  end
end

macro ecs(components, systems)
  def initialize
    {% for component in components %}
      @components[:{{ component.names.last.underscore }}] = StaticArray(ECS::Component, ENTITY_COUNT).new { {{ component }}.new }
    {% end %}

    {% for system in systems %}
      @systems << {{ system }}.new(self)
    {% end %}
  end

  {% for component in components %}
    def component_{{ component.names.last.underscore }}(entity_id : Int32)
      @components[:{{ component.names.last.underscore }}][entity_id].as({{ component }})
    end
  {% end %}
end
